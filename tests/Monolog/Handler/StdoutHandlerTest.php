<?php

namespace Monolog\Handler;

use Monolog\Formatter\ColorLineFormatter;
use Monolog\Logger;
use Monolog\Test\TestCase;

class StdoutHandlerTest extends TestCase
{
    public function testDefaults()
    {
        $handler = new StdoutHandler();

        $this->assertSame(Logger::DEBUG, $handler->getLevel());
        $this->assertTrue($handler->getFormatter() instanceof ColorLineFormatter);
    }
}
