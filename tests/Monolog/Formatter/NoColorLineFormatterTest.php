<?php

namespace Monolog\Formatter;

use Monolog\Handler\StdoutHandler;
use Monolog\Logger;
use Monolog\Test\TestCase;

class NoColorLineFormatterTest extends TestCase
{
    private $formatter;

    protected function setUp(): void
    {
        $this->formatter = new NoColorLineFormatter(StdoutHandler::FORMAT);
    }

    private function getFormattedMessage($colorName)
    {
        $message = sprintf('[info][c=%1$s]black[/c] and [c=%1$s]white[/c]!', $colorName);

        return $this->formatter->format(
            $this->getRecord(Logger::INFO, $message)
        );
    }

    /**
     * @dataProvider providerTestColor
     */
    public function testRealColor($colorName)
    {
        $expected = "[info]black and white!\n";
        $this->assertSame($expected, $this->getFormattedMessage($colorName));
    }

    public function providerTestColor()
    {
        return [
            ['black'],
            ['red'],
            ['green'],
            ['yellow'],
            ['blue'],
            ['purple'],
            ['cyan'],
            ['white'],
            ['starwars'],
            ['color'],
            ['other'],
            ['null'],
        ];
    }

    public function testOtherCode()
    {
        $message = '[[c=red]/!\[/c]]use [u][c=blue]no color[/c][/u] with care!';
        $expected = "[/!\]use [u]no color[/u] with care!\n";

        $this->assertSame($expected, $this->formatter->format($this->getRecord(Logger::ERROR, $message)));
    }
}
