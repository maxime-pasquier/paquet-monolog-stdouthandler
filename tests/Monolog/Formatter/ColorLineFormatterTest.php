<?php

namespace Monolog\Formatter;

use Monolog\Test\TestCase;
use Monolog\Logger;
use Monolog\Handler\StdoutHandler;

class ColorLineFormatterTest extends TestCase
{
    private $formatter;

    protected function setUp(): void
    {
        $this->formatter = new ColorLineFormatter(StdoutHandler::FORMAT);
    }

    private function getFormattedMessage($colorName)
    {
        $message = sprintf('[error][c=%s]core dumped[/c].', $colorName);

        return $this->formatter->format(
            $this->getRecord(Logger::ERROR, $message)
        );
    }

    /**
     * @dataProvider providerTestColor
     */
    public function testRealColor($colorName, $colorValue)
    {
        $expected = sprintf("[error]\033[%dmcore dumped\033[0m.\n", $colorValue);
        $this->assertSame($expected, $this->getFormattedMessage($colorName));
    }

    public function providerTestColor()
    {
        return [
            ['none',   0],
            ['black',  30],
            ['red',    31],
            ['green',  32],
            ['yellow', 33],
            ['blue',   34],
            ['purple', 35],
            ['cyan',   36],
            ['white',  37],
        ];
    }

    /**
     * @dataProvider providerTestUnknownColor
     */
    public function testUnknownColor($colorName)
    {
        $expected = "[error]\033[0mcore dumped\033[0m.\n";
        $this->assertSame($expected, $this->getFormattedMessage($colorName));
    }

    public function providerTestUnknownColor()
    {
        return [
            ['foo'],
            ['bar'],
        ];
    }

    /**
     * @dataProvider providerTestBadValue
     */
    public function testBadValue($colorName)
    {
        $expected = sprintf("[error][c=%s]core dumped\033[0m.\n", $colorName);
        $this->assertSame($expected, $this->getFormattedMessage($colorName));
    }

    public function providerTestBadValue()
    {
        return [
            [null],
            [''],
            [' '],
            ['red2'],
            ['red-light'],
            ['dark_red'],
        ];
    }

    public function testOtherCode()
    {
        $message = '[[c=yellow]warning[/c]][c=green][b]huge[/b] [comment]packet[/comment][/c] [c=white]is coming[/c].';
        $expected = "[\033[33mwarning\033[0m]\033[32m[b]huge[/b] [comment]packet[/comment]\033[0m \033[37mis coming\033[0m.\n";

        $this->assertSame($expected, $this->formatter->format($this->getRecord(Logger::ERROR, $message)));
    }

    /**
     * @dataProvider providerTestInvalidRenderShellColor
     */
    public function testInvalidRenderShellColor($id)
    {
        $this->expectException(\LogicException::class);

        ColorLineFormatter::renderShellColor($id);
    }

    public function providerTestInvalidRenderShellColor()
    {
        return [
            [null],
            [''],
            [' '],
            [true],
            [false],
            [new \StdClass()],
        ];
    }
}
