# Gestionnaire de message pour Monolog (Monolog Stdout Handler)

[![Intégration continue : Bitbucket Pipelines](https://img.shields.io/bitbucket/pipelines/maxime-pasquier/paquet-monolog-stdouthandler/main?style=flat-square&logo=bitbucket)][1]
[![Nombre de téléchargement : Packagist](https://img.shields.io/packagist/dt/maxime-pasquier/monolog-stdout-handler?style=flat-square&color=f28d1a)][2]
[![Dernière version stable : Packagist](https://img.shields.io/packagist/v/maxime-pasquier/monolog-stdout-handler?style=flat-square&color=f28d1a)][2]
[![Versions PHP supportées](https://img.shields.io/packagist/php-v/maxime-pasquier/monolog-stdout-handler?style=flat-square&color=8892bf)][3]
![License](https://img.shields.io/packagist/l/maxime-pasquier/monolog-stdout-handler?style=flat-square)


Gestionnaire pour [Monolog][4] qui permet d'envoyer des messages en couleurs sur la sortie standard (stdout).

La couleur des messages peut être désactivée avec un formateur fourni.


Les loggers sont capables d'interpréter un langage de balise (comme le bbcode).

Les balises actuellement reconnues sont :

 * `[c=<color>]...[/c]` avec les couleurs : `black`, `blue`, `green`, `cyan`, `red`, `purple`, `yellow`, `white`

## Exemple

Utilisation du gestionnaire de message :

```php
<?php
use Monolog\Logger;
use Monolog\Handler\StdoutHandler;

$stdoutHandler = new StdoutHandler();
$logger = new Logger('cronjob');
$logger->pushHandler($stdoutHandler);

$logger->error('[c=green]Hello world![/c]');
```

Désactivation de la couleur :

```php
<?php
use Monolog\Formatter\NoColorLineFormatter;

$stdoutHandler->setFormatter(new NoColorLineFormatter(StdoutHandler::FORMAT));
```


  [1]: https://bitbucket.org/maxime-pasquier/paquet-monolog-stdouthandler/addon/pipelines/home
  [2]: https://packagist.org/packages/maxime-pasquier/monolog-stdout-handler
  [3]: https://www.php.net/supported-versions.php
  [4]: https://github.com/Seldaek/monolog
